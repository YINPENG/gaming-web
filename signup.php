<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style1.css" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signup.css" rel="stylesheet">
    <title>Sign Up</title>
  </head>

  <body>
  <div class="container">
    <form class="signup" method="POST" action="signupdata.php" enctype="multipart/form-data">
    <h1 class="text-center">Create Your Account</h1>
    <div class="form-group">
      <label>Email address</label>
      <input type="email" name="emailaddress" class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter your email address" required>
      <small id="emailHelp" class="form-text text-muted">Your email address will be used to log in.</small>
    </div>
    <div class="form-group">
      <label>Password</label>
      <input type="password" name="password" class="form-control" id="InputPassword1" placeholder="Set your password" required>
      <small id="passwordHelp" class="form-text text-muted">Always keep your password safe.</small>
    </div>
    <div class="form-group">
      <label>First Name</label>
      <input type="text" name="firstname" class="form-control" id="InputFirstName" placeholder="First Name" required>
    </div>
    <div class="form-group">
      <label>Last Name</label>
      <input type="text" name="lastname" class="form-control" id="InputLastName" placeholder="Last Name" required>
    </div>
    <fieldset class="form-group">
      <label>Gender</label>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="male" checked>
          Male
        </label>
      </div>
      <div class="form-check">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="female">
          Female
        </label>
      </div>
    </fieldset>
    <div class="form-group">
      <label>Select Your Region</label>
      <select name="region" class="form-control" id="Select1" required>
        <option>North America</option>
        <option>European Union</option>
        <option>Asia</option>
        <option>Oceania</option>
        <option>South America</option>
      </select>
    </div>

    <div class="upload">
    <h3>Upload Profile Picture</h3>
      <div class="imageBox">
        <div class="thumbBox"></div>
        <div class="spinner" style="display: none">Loading...</div>
      </div>
      <div class="action"> 
        <div class="new-contentarea tc"> 
          <a href="javascript:void(0)" class="upload-img">
              <label for="upload-file">Upload Image</label>
          </a>
          <input type="file" class="" name="upload-file" id="upload-file" />
        </div>
        <input type="button" id="btnCrop"  class="Btnsty_peyton" value="Cut">
        <input type="button" id="btnZoomIn" class="Btnsty_peyton" value="+"  >
        <input type="button" id="btnZoomOut" class="Btnsty_peyton" value="-" >
      </div>
      <div class="cropped"></div>
      <script src="http://cdn.bootcss.com/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
	    <script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
      <script type="text/javascript" src="js/cropbox.js"></script>
      <script type="text/javascript">
      $(window).load(function() {
        var options =
        {
          thumbBox: '.thumbBox',
          spinner: '.spinner',
          imgSrc: 'img/avatar.jpg'
        }
        var cropper = $('.imageBox').cropbox(options);
        $('#upload-file').on('change', function(){
          var reader = new FileReader();
          reader.onload = function(e) {
            options.imgSrc = e.target.result;
            cropper = $('.imageBox').cropbox(options);
          }
          reader.readAsDataURL(this.files[0]);
          this.files = [];
        })
        $('#btnCrop').on('click', function(){
          var img = cropper.getDataURL();
          $('.cropped').html('');
          $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:64px;margin-top:4px;border-radius:64px;box-shadow:0px 0px 12px #7E7E7E;" ><p>64px*64px</p>');
          $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:128px;margin-top:4px;border-radius:128px;box-shadow:0px 0px 12px #7E7E7E;"><p>128px*128px</p>');
          $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0px 0px 12px #7E7E7E;"><p>180px*180px</p>');
        })
        $('#btnZoomIn').on('click', function(){
          cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
          cropper.zoomOut();
        })
      });
      </script>
    </div>  

    <button type="submit" class="btn btn-primary" id="submitButton">Submit</button>
    </form>
  </div>
  </body>
	
</html>