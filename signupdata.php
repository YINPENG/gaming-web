<?php
    $emailaddress=$_REQUEST['emailaddress'];
    $mail = $emailaddress;
    $emailaddress="'".$emailaddress."'";
    $password=$_REQUEST['password'];
    $password= password_hash($password, PASSWORD_DEFAULT);
    $password="'".$password."'";
    $firstname=$_REQUEST['firstname'];
    $firstname="'".$firstname."'";
    $lastname=$_REQUEST['lastname'];
    $lastname="'".$lastname."'";
    $optionsRadios=$_REQUEST['optionsRadios'];
    $optionsRadios="'".$optionsRadios."'";
    $region=$_REQUEST['region'];
    $imagename=$_FILES["upload-file"]["name"];
    
    //if user didn't upload image, set it to logo
    if($imagename == null){
        $imagename = 'logo.png';
    }
    $imagename="'".$imagename."'";
    
    if($region == "North America"){
        $region = 1;
    }
    if($region == "European Union"){
        $region = 2;
    }
    if($region == "Asia"){
        $region = 3;
    }
    if($region == "Oceania"){
        $region = 4;
    }
    if($region == "South America"){
        $region = 5;
    }

    echo $emailaddress;
    echo "<br>";
    echo $password;
    echo "<br>";
    echo $firstname;
    echo "<br>";
    echo $lastname;
    echo "<br>";
    echo $optionsRadios;
    echo "<br>";
    echo $region;
    echo "<br>";
    echo $imagename;
    echo "<br>";


    class MySQLDatabase{
        public $link = null;
        function connect($user, $password, $database){
            $this->link = mysqli_connect('localhost', $user, $password);
            if(!$this->link){
            die('Not connected : ' . mysqli_connect_error());
            }
            $db = mysqli_select_db($this->link, $database);
            if(!$db){
            die ('Cannot use : ' . $database);
            }
            return $this->link;
        }
    
        function disconnect(){ 
            mysqli_close($this->link);
        }
    }
    
    $db=new MySQLDatabase();
    $db->connect("admin","password","database");
    $query = "insert into user_accounts ( email, password, first_name, last_name, gender, region, picture_name) values ($emailaddress, $password, $firstname, $lastname, $optionsRadios, $region, $imagename)";
    $result = mysqli_query($db->link,$query);
    if(!$result){
    die(mysqli_error($db->link)); 
    }

    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["upload-file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["upload-file"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["upload-file"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["upload-file"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }


   // the message
   $msg = "Hi,\nThank you for joining us!\n\nGameHub";

   // use wordwrap() if lines are longer than 70 characters
   $msg = wordwrap($msg,70);

   // send email
   mail($mail ,"GameHub",$msg);


   header("Location: signupsuccess.php");

?>