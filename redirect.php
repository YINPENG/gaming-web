<!-- this page is to check whether user enter a correct email and password and redirect to other page. -->
<?php

    $email=$_REQUEST['email'];
    $password=$_REQUEST['password'];
    $is_right="wrong";
    class MySQLDatabase{
        public $link = null;
        function connect($user, $password, $database){
            $this->link = mysqli_connect('localhost', $user, $password);
            if(!$this->link){
            die('Not connected : ' . mysqli_connect_error());
            }
            $db = mysqli_select_db($this->link, $database);
            if(!$db){
            die ('Cannot use : ' . $database);
            }
            return $this->link;
        }
    
        function disconnect(){ 
            mysqli_close($this->link);
        }
    }
    
    $db=new MySQLDatabase();
    $db->connect("admin","password","database");
    $query = "select * from user_accounts";
    $result = mysqli_query($db->link,$query);
    if($result){
        while($row = mysqli_fetch_array($result)){
            if($email==$row['email'] && password_verify($password, $row['password'])){
                $is_right = "right";
                $first_name = $row['first_name'];
                $picture_path = $row['picture_name'];
            }
        }
    }
    else{
    die(mysqli_error($db->link)); // useful for debugging
    }

    //redirect
    if($is_right=="right"){
        session_start();
        $_SESSION['email'] = $_REQUEST['email'];
        $_SESSION['first_name'] = $first_name;
        $_SESSION['picture_path'] = $picture_path;
        header("Location: loginSuccess.php");
    }else{
        header("Location: loginFail.php");
    }
    
                
?>