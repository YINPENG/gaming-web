<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome To GameHub</title>
	<link rel="stylesheet" type="text/css" href="css/flickity.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="js/modernizr.custom.js"></script>
</head>

<body>
	<div class="container">
		<div class="hero">
			<div class="hero__back hero__back--static"></div>
			<div class="hero__back hero__back--mover"></div>
			<div class="hero__front"></div>
		</div>
		<header class="codrops-header">
			<h1 class="codrops-title">Game Hub</h1>
			<nav class="menu">
			<?php 
				session_start();
				$log_out = '<a href="logout.php"><span style="color:white; font-weight:bold; margin-left:5px;">Log Out</span></a>';
				$log_in = '<a href="login.php"><span style="color:white; font-weight:bold;">Log In</span></a>';
				if(isset($_SESSION['email']) && isset($_SESSION['first_name'])){
					$profile = 'uploads/'.$_SESSION['picture_path'];
					echo '<img width="30px" height="30px" src="'.$profile.'"></img>';
					echo '    Hi,  '.$_SESSION['first_name'].' .  '.$log_out;
				}else{
					echo $log_in;
				}
			?>
				<a class="menu__item menu__item--current" href=""><span>Game Hub</span></a>
				<a class="menu__item" href="comment.php"><span style="color:white;">Chat</span></a>
			</nav>
		</header>
		<div class="stack-slider">
			<div class="stacks-wrapper">
			<div class="stack">
					<h2 class="stack-title"><a href="#" data-text="Massively Multiplayer Online"><span>Multiplayer Online</span></a></h2>
					<div class="item">
						<div class="item__content">
							<?php
								include("db.php");
								$sql = "select * from multiplayer";
								$result = mysqli_query($db->link,$sql);
								while($data=mysqli_fetch_array($result)){
							?>				
								<img src="<?php echo $data['url'];?>" style="width:600px; height:400px;" >
								<h4 class="item__title"><?php echo $data['name'];?><span class="item__date"><?php echo $data['update_time'];?></span><br><i style="color:red;" class="fa fa-heart"></i><a style="margin-right:10px;"class="multiplayer" title="like" rel="<?php echo $data['id'];?>" onclick="likeplus()"><?php echo $data['num'];?></a>Likes</h4>														
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="stack">
					<h2 class="stack-title"><a href="#" data-text="Role Playing"><span>Role Playing</span></a></h2>
					<div class="item">
						<div class="item__content">
							<?php						
								$sql = "select * from roleplaying";
								$result = mysqli_query($db->link,$sql);
								while($data=mysqli_fetch_array($result)){
							?>				
								<img src="<?php echo $data['url'];?>" style="width:600px; height:400px;" >
								<h4 class="item__title"><?php echo $data['name'];?><span class="item__date"><?php echo $data['update_time'];?></span><br><i style="color:red;" class="fa fa-heart"></i><a style="margin-right:10px;"class="roleplaying" title="like" rel="<?php echo $data['id'];?>" onclick="likeplus()"><?php echo $data['num'];?></a>Likes</h4>														
							<?php } ?>
						</div>
					</div>			
				</div>
				<div class="stack">
					<h2 class="stack-title"><a href="#" data-text="Sports"><span>Sports</span></a></h2>
					<div class="item">
						<div class="item__content">
							<?php						
								$sql = "select * from sports";
								$result = mysqli_query($db->link,$sql);
								while($data=mysqli_fetch_array($result)){
							?>				
								<img src="<?php echo $data['url'];?>" style="width:600px; height:400px;" >
								<h4 class="item__title"><?php echo $data['name'];?><span class="item__date"><?php echo $data['update_time'];?></span><br><i style="color:red;" class="fa fa-heart"></i><a style="margin-right:10px;"class="sports" title="like" rel="<?php echo $data['id'];?>" onclick="likeplus()"><?php echo $data['num'];?></a>Likes</h4>													
							<?php } ?>
						</div>
					</div>			
				</div>
				<div class="stack">
					<h2 class="stack-title"><a href="#" data-text="Casual"><span>Casual</span></a></h2>
					<div class="item">
						<div class="item__content">
							<?php						
								$sql = "select * from casual";
								$result = mysqli_query($db->link,$sql);
								while($data=mysqli_fetch_array($result)){
							?>				
								<img src="<?php echo $data['url'];?>" style="width:600px; height:400px;" >
								<h4 class="item__title"><?php echo $data['name'];?><span class="item__date"><?php echo $data['update_time'];?></span><br><i style="color:red;" class="fa fa-heart"></i><a style="margin-right:10px;"class="casual" title="like" rel="<?php echo $data['id'];?>" onclick="likeplus()"><?php echo $data['num'];?></a>Likes</h4>													
							<?php } ?>
						</div>
					</div>			
				</div>
			</div>

		</div>
			<!-- /stacks-wrapper -->
		</div>
		<!-- /stacks -->
		<img class="loader" src="image/three-dots.svg" width="60" alt="Loader image" />
	</div>
	<!-- /container -->
	<script typet="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script src="js/flickity.pkgd.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/main.js"></script>
	<!-- <script src="https://www.youhutong.com/static/js/jquery.js"></script> -->
	<script typet="text/javascript" src="js/javas.js"></script>

</body>
</html>